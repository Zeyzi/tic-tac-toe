class TicTacToe {
  private PLAYER_X_CLASS = "x";
  private PLAYER_O_CLASS = "circle";
  private WINNING_COMBINATIONS = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];

  private cellElements: NodeListOf<Element>;
  private boardElement: HTMLElement;
  private winningMessageElement: HTMLElement;
  private restartButton: HTMLElement;
  private winningMessageTextElement: HTMLElement;
  private isPlayerOTurn: boolean;

  constructor() {
    this.cellElements = document.querySelectorAll("[data-cell]");
    this.boardElement = document.getElementById("board")!;
    this.winningMessageElement = document.getElementById("winningMessage")!;
    this.restartButton = document.getElementById("restartButton")!;
    this.winningMessageTextElement =
      document.getElementById("winningMessageText")!;
    this.isPlayerOTurn = false;

    this.startGame();
    this.restartButton.addEventListener("click", () => this.startGame());
  }

  private startGame() {
    this.isPlayerOTurn = false;
    this.cellElements.forEach((cell) => {
      cell.classList.remove(this.PLAYER_X_CLASS);
      cell.classList.remove(this.PLAYER_O_CLASS);
      cell.removeEventListener("click", this.handleCellClick);
      cell.addEventListener("click", this.handleCellClick, { once: true });
    });
    this.setBoardHoverClass();
    this.winningMessageElement.classList.remove("show");
  }

  private handleCellClick = (e: Event) => {
    const cell = e.target as HTMLElement;
    const currentClass = this.isPlayerOTurn
      ? this.PLAYER_O_CLASS
      : this.PLAYER_X_CLASS;
    this.placeMark(cell, currentClass);
    if (this.checkWin(currentClass)) {
      this.endGame(false);
    } else if (this.isDraw()) {
      this.endGame(true);
    } else {
      this.swapTurns();
      this.setBoardHoverClass();
    }
  };

  private isDraw() {
    return [...this.cellElements].every(
      (cell) =>
        cell.classList.contains(this.PLAYER_X_CLASS) ||
        cell.classList.contains(this.PLAYER_O_CLASS)
    );
  }

  private placeMark(cell: HTMLElement, currentClass: string) {
    cell.classList.add(currentClass);
  }

  private swapTurns() {
    this.isPlayerOTurn = !this.isPlayerOTurn;
  }

  private setBoardHoverClass() {
    this.boardElement.classList.remove(
      this.PLAYER_X_CLASS,
      this.PLAYER_O_CLASS
    );
    this.boardElement.classList.add(
      this.isPlayerOTurn ? this.PLAYER_O_CLASS : this.PLAYER_X_CLASS
    );
  }

  private checkWin(currentClass: string) {
    return this.WINNING_COMBINATIONS.some((combination) =>
      combination.every((index) =>
        this.cellElements[index].classList.contains(currentClass)
      )
    );
  }

  private endGame(draw: boolean) {
    if (draw) {
      this.winningMessageTextElement.innerText = "It's a draw!";
    } else {
      this.winningMessageTextElement.innerText = `Player with ${
        this.isPlayerOTurn ? "O's" : "X's"
      } wins`;
    }
    this.winningMessageElement.classList.add("show");
  }
}

new TicTacToe();