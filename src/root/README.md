# Tic-Tac-Toe

Welcome to the **Tic-Tac-Toe** game project! This classic game allows two players to engage in a friendly match, with one player as "X" and the other as "O". The game automatically determines the winner. Enjoy playing!

## Table of Contents

- [Getting Started](#getting-started)
- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [Running the Project](#running-the-project)
- [Playing the Game](#playing-the-game)
- [Contributing](#contributing)

## Getting Started

Follow the instructions below to set up and run the project on your local machine.

## Prerequisites

Ensure you have the following installed on your computer:
- [Node.js](https://nodejs.org/)
- [Visual Studio Code](https://code.visualstudio.com/)
- [TypeScript](https://www.typescriptlang.org/)

## Installation

1. **Clone the repository** to your local machine:
   ```bash
   git clone https://github.com/zeyzi/tic-tac-toe.git
   ```
2. **Navigate to the project directory**:
   ```bash
   cd tic-tac-toe
   ```
3. **Install the necessary dependencies**:
   ```bash
   npm install
   ```

## Running the Project

1. **Open the Terminal**:

   Open a terminal in your code editor. For Visual Studio Code, you can use the integrated terminal.

   - **Windows**: `Ctrl + ` (backtick)
   - **Mac**: `Control + ` (backtick)

2. **Compile TypeScript**:

   Start the TypeScript compiler in watch mode to automatically compile your TypeScript files when they change.

   ```bash
   tsc --watch
   ```

3. **Start the Live Server**:

   Open the `index.html` file in Visual Studio Code. Right-click anywhere in the file and select "Open with Live Server". If you do not have the Live Server extension installed, you can install it from the Visual Studio Code marketplace.

## Playing the Game

Once the live server is running, your default web browser should open the game. If not, navigate to `http://127.0.0.1:5500` (or the address provided by Live Server).

## Contributing

Contributions are welcome! If you'd like to contribute to this project, please fork the repository and create a pull request. Feel free to open issues for any bugs or feature requests.

Thank you for checking out the **Tic-Tac-Toe** project. We hope you enjoy playing the game!
```